package auth

import (
	"fmt"
	"time"

	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"

	"basic_gin/config"
	"basic_gin/models"

	"github.com/golang-jwt/jwt/v4"
)

// //Biding from JSON
// type RegisterBody struct{
// 	ID string `json:"id" binding:"required"`
// 	Password string `json:"Password" binding:"required"`
// 	Case int `gorm:"type:int" json:"Case" binding:"required"`
// 	IsDelete int `gorm:"type:int" json:"IsDelete" binding:"required"`
// }

// func Register(c *gin.Context){
// 	var json RegisterBody
// 	if err := c.ShouldBindJSON(&json); err !=nil{
// 		c.JSON(http.StatusBadRequest, gin.H{"error":err.Error()})
// 		return
// 	}
// 	//Check user exits
// 	var userExit models.User
// 	config.DB.Where("id = ?", json.ID).First(&userExit)
// 	if(len(userExit.ID)>0){
// 		c.JSON(http.StatusOK, gin.H{"status": "error","message":"User already exit"})
// 		return
// 	}
// 	c.JSON(200, gin.H{
// 		"register" :json,
// 	})
// }

var mySigningKey []byte
//Binding from JSON
type LoginBody struct{
	ID string `json:"id" binding:"required"`
	Password string `json:"Password" binding:"required"`
}

func Login(c *gin.Context){
	var json LoginBody
	if err := c.ShouldBindJSON(&json); err !=nil{
		c.JSON(http.StatusBadRequest, gin.H{"error":err.Error()})
		return
	}
	//Check user exits
	var userExit models.User
	config.DB.Where("id = ?", json.ID).First(&userExit)
	if(len(userExit.ID) == 0){
		c.JSON(http.StatusOK, gin.H{"status": "error","message":"User does not exit"})
		return
	}
	err :=bcrypt.CompareHashAndPassword([]byte(userExit.Password),[]byte(json.Password))

	if userExit.Case==4 {
		var customerExit models.Customer
		config.DB.Where("user_id = ?", userExit.ID).First(&customerExit)
		var contractExit models.Contract
		config.DB.Where("customer_id = ?", customerExit.ID).Order("end_date asc").Find(&contractExit)
		if err == nil {
			mySigningKey  = []byte("secretkey")
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"userId":userExit.ID,
				"Case":userExit.Case,
				"customerId":customerExit.ID,
				"contract":contractExit,
				"exp": time.Now().Add(time.Minute*60).Unix(),
				
			})
			tokenString, err := token.SignedString((mySigningKey))
			fmt.Println(tokenString, err)

			c.JSON(http.StatusOK,gin.H{"status":"ok","message":"Login Success","token":tokenString})
			}else{
				c.JSON(http.StatusOK,gin.H{"status":"error","message":"Login Failed"})
			}
	} else {
		var employeeExit models.Employee
		config.DB.Where("user_id = ?", userExit.ID).First(&employeeExit)

		if err == nil {
			mySigningKey  = []byte("secretkey")
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"userId":userExit.ID,
				"Case":userExit.Case,
				"employeeId":employeeExit.ID,
				"exp": time.Now().Add(time.Minute*60).Unix(),
				
			})
			tokenString, err := token.SignedString((mySigningKey))
			fmt.Println(tokenString, err)

			c.JSON(http.StatusOK,gin.H{"status":"ok","message":"Login Success","token":tokenString})
			}else{
				c.JSON(http.StatusOK,gin.H{"status":"error","message":"Login Failed"})
			}
	}


}