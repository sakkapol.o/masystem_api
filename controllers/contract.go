package controllers

import (
	"basic_gin/config"
	"basic_gin/models"
	//"fmt"
	"net/http"
	"github.com/gin-gonic/gin"
)

func GetContractLists(c *gin.Context){
	var contract []models.Contract
	err := models.GetAllContractLists(&contract)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}else{
		c.JSON(http.StatusOK,contract)
	}
}

func GetContractDetails(c *gin.Context){
	id := c.Params.ByName("id")
	var contractdetail models.Contract
	err := models.GetContractDetails(&contractdetail, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}else{
		c.JSON(http.StatusOK,contractdetail)
	}
}

func PostContract(c *gin.Context){
	var contract models.Contract
	c.ShouldBindJSON(&contract)
	if err := models.PostContract(&contract) ; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data": contract,
	
	})
}

func UpdateContract(c *gin.Context){
	var contract models.Contract
	if err := config.DB.Where("id = ?", c.Param("id")).First(&contract).Error; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	var input models.Contract
	if err := c.ShouldBindJSON(&input); err != nil{
		c.JSON(http.StatusBadRequest,gin.H{"error":err.Error()})
		return
	}
	config.DB.Model(&contract).Updates(input)
	c.JSON(http.StatusOK, gin.H{"data": input})
}

func DeleteContract(c *gin.Context){
	var contract models.Contract
	if err := config.DB.Where("id = ?", c.Param("id")).First(&contract).Error; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	var input models.Contract
	input.IsDelete = 1
	config.DB.Model(&contract).Updates(input)
	c.JSON(http.StatusOK, gin.H{"data": input})
}

func GetContract(c *gin.Context){
	customerid := c.Params.ByName("CustomerId")
	var contractdetail models.Contract
	err := models.GetContract(&contractdetail, customerid)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}else{
		c.JSON(http.StatusOK,contractdetail)
	}
}