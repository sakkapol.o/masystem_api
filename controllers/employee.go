package controllers

import (
	"basic_gin/models"
	//"fmt"
	"net/http"
	"github.com/gin-gonic/gin"
)

func GetAllEmployeeLists(c *gin.Context){
	var employee []models.Employee
	err := models.GetAllEmployeeLists(&employee)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}else{
		c.JSON(http.StatusOK,employee)
	}
}
