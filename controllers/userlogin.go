package controllers

import (
	"basic_gin/config"
	"basic_gin/models"

	//"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func GetUserLists(c *gin.Context){
	var user []models.User
	err := models.GetAllUserLists(&user)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}else{
		c.JSON(http.StatusOK,user)
	}
}

func GetUserDetails(c *gin.Context){
	id := c.Params.ByName("id")
	var userdetails models.User
	err := models.GetUserDetails(&userdetails, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}else{
		c.JSON(http.StatusOK,userdetails)
	}
}

func PostUser(c *gin.Context){
	var user models.User
	c.ShouldBindJSON(&user)
	encrytedPassword, _:= bcrypt.GenerateFromPassword([]byte(user.Password),10)
	userBody := models.User{ID:user.ID,Password:string(encrytedPassword),Case:user.Case}
	if err := models.PostUser(&userBody) ; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data": user,
		"encrytedPassword": encrytedPassword,
	
	})
}

func UpdateUser(c *gin.Context){
	var user models.User
	if err := config.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	if err := c.ShouldBindJSON(&user); err != nil{
		c.JSON(http.StatusBadRequest,gin.H{"error":err.Error()})
		return
	}
	encrytedPassword, _:= bcrypt.GenerateFromPassword([]byte(user.Password),10)
	userBody := models.User{ID:user.ID,Password:string(encrytedPassword),Case:user.Case}
	config.DB.Model(&user).Updates(&userBody)
	c.JSON(http.StatusOK, gin.H{"data": userBody})
}

func DeleteUser(c *gin.Context){
	var user models.User
	if err := config.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	var input models.User
	input.IsDelete = 1
	config.DB.Model(&user).Updates(input)
	c.JSON(http.StatusOK, gin.H{"data": input})
}