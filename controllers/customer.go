package controllers

import (
	"basic_gin/config"
	"basic_gin/models"
	//"fmt"
	"net/http"
	"github.com/gin-gonic/gin"
)

func GetCustomerLists(c *gin.Context){
	var customer []models.Customer
	err := models.GetAllCustomerLists(&customer)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}else{
		c.JSON(http.StatusOK,customer)
	}
}

func GetCustomerDetails(c *gin.Context){
	id := c.Params.ByName("id")
	var customerdetail models.Customer
	err := models.GetCustomerDetails(&customerdetail, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}else{
		c.JSON(http.StatusOK,customerdetail)
	}
}

func PostCustomer(c *gin.Context){
	var customer models.Customer
	c.ShouldBindJSON(&customer)
	if err := models.PostCustomer(&customer) ; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data": customer,
	
	})
}

func UpdateCustomer(c *gin.Context){
	var customer models.Customer
	if err := config.DB.Where("id = ?", c.Param("id")).First(&customer).Error; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	var input models.Customer
	if err := c.ShouldBindJSON(&input); err != nil{
		c.JSON(http.StatusBadRequest,gin.H{"error":err.Error()})
		return
	}
	config.DB.Model(&customer).Updates(input)
	c.JSON(http.StatusOK, gin.H{"data": input})
}

func DeleteCustomer(c *gin.Context){
	var customer models.Customer
	if err := config.DB.Where("id = ?", c.Param("id")).First(&customer).Error; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	var input models.Customer
	input.IsDelete = 1
	config.DB.Model(&customer).Updates(input)
	c.JSON(http.StatusOK, gin.H{"data": input})
}

// func GetCustomer(c *gin.Context){
// 	userid := c.Params.ByName("UserID")
// 	var customerdetail models.Customer
// 	err := models.GetCustomer(&customerdetail, userid)
// 	if err != nil {
// 		c.AbortWithStatus(http.StatusNotFound)
// 	}else{
// 		c.JSON(http.StatusOK,customerdetail)
// 	}
// }