package controllers

import (
	"basic_gin/config"
	"basic_gin/models"

	//"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetIssuesLists(c *gin.Context){
	var issue []models.Issue
	err := models.GetAllIssueLists(&issue)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}else{
		c.JSON(http.StatusOK,issue)
	}
}

func GetIssuesDetails(c *gin.Context){
	id := c.Params.ByName("id")
	var IssueDetail models.Issue
	err := models.GetIssueDetails(&IssueDetail, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	}else{
		c.JSON(http.StatusOK,IssueDetail)
	}
}

func PostIssue(c *gin.Context){
	var issue models.Issue
	c.ShouldBindJSON(&issue)
	if err := models.PostIssue(&issue) ; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// if err := config.DB.Select("IssueDetail","IssueDate","IssueStatus","IssueLevel","CustomerId","EmployeeId").Create(&Issue).Error ; err != nil{
	// 	c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	// 	return
	// }
	c.JSON(http.StatusOK, gin.H{
		"status": http.StatusOK,
		"data": issue,
	
	})
}

func UpdateIssue(c *gin.Context){
	var issue models.Issue
	if err := config.DB.Where("id = ?", c.Param("id")).First(&issue).Error; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	var input models.Issue
	if err := c.ShouldBindJSON(&input); err != nil{
		c.JSON(http.StatusBadRequest,gin.H{"error":err.Error()})
		return
	}
	config.DB.Model(&issue).Updates(input)
	c.JSON(http.StatusOK, gin.H{"data": input})
}

func DeleteIssue(c *gin.Context){
	var issue models.Issue
	if err := config.DB.Where("id = ?", c.Param("id")).First(&issue).Error; err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	var input models.Issue
	input.IssueStatus = "canceled"
	// if err := c.ShouldBindJSON(&input); err != nil{
	// 	c.JSON(http.StatusBadRequest,gin.H{"error":err.Error()})
	// 	return
	// }
	config.DB.Model(&issue).Updates(input)
	c.JSON(http.StatusOK, gin.H{"data": input})
}