package main

import (

	"basic_gin/config"
	"basic_gin/models"
	"basic_gin/routers"

	"fmt"
	//"net/http"

	// "github.com/gin-contrib/cors"
	"github.com/jinzhu/gorm"
)

var err error

// func handler(w http.ResponseWriter, req *http.Request) {
//     // ...
// 	enableCors(&w)
//     // ...
// }

// func enableCors(w *http.ResponseWriter) {
// 	(*w).Header().Set("Access-Control-Allow-Origin", "*")
// }


func main() {

	config.DB, err = gorm.Open("mysql", config.DbURL(config.BuildDBConfig()))

	if err != nil {
		fmt.Println("Status", err)
	}
	defer config.DB.Close()
	config.DB.AutoMigrate(&models.Issue{})
	config.DB.AutoMigrate(&models.Contract{})
	config.DB.AutoMigrate(&models.Customer{})
	config.DB.AutoMigrate(&models.Employee{})
	config.DB.AutoMigrate(&models.User{})


	r := routers.SetupRouter()


	
	r.Run()
}
