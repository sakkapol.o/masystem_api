package config

import (
	"fmt"
	"github.com/jinzhu/gorm"

  )
var (
	DB *gorm.DB

) 
// func DBOpen() {
// 	db, err := gorm.Open("mysql", "root:1234@/masystem?charset=utf8&parseTime=True&loc=Local")
// 	if err !=nil {
// 		panic(err)
// 	}
	
// 	defer db.Close()

// }

//func DBClose(){
	//db.Close()
//}

// DBConfig represents db configuration
type DBConfig struct {
	Host     string
	Port     int
	User     string
	DBName   string
	Password string
   }
   func BuildDBConfig() *DBConfig {
	dbConfig := DBConfig{
	 Host:     "localhost",
	 Port:     3306,
	 User:     "root",
	 Password: "1234",
	 DBName:   "masystem",
	}
	return &dbConfig
   }
   func DbURL(dbConfig *DBConfig) string {
	return fmt.Sprintf(
	 "%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
	 dbConfig.User,
	 dbConfig.Password,
	 dbConfig.Host,
	 dbConfig.Port,
	 dbConfig.DBName,
	)
   }