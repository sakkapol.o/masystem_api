package models

type Customer struct {
	ID int `gorm:"primary_key:autoincrement" json:"id"`
	CustomerName string `gorm:"type:varchar(40)" json:"CustomerName"`
	CustomerAddress string `gorm:"type:varchar(200)" json:"CustomerAddress"`
	ContactName string `gorm:"type:varchar(100)" json:"ContactName"`
	Telephone string `gorm:"type:varchar(10)" json:"Telephone"`
	UserID string `gorm:"type:varchar(20)" json:"UserID"`
	EmployeeId string `gorm:"type:varchar(6)" json:"EmployeeId"`
	IsDelete int `gorm:"type:int" json:"IsDelete"`
}

func (b *Issue) Customer() string {
	return "customer"
}