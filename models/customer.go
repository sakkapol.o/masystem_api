package models

import(
	"basic_gin/config"
	//"fmt"
	_ "github.com/go-sql-driver/mysql"
)

func GetAllCustomerLists(customer *[]Customer)(err error){
	if err = config.DB.Where("is_delete = ?", "0").Find(&customer).Error; err != nil{
		return err
	}
	return nil
}

func GetCustomerDetails(customer *Customer, id string)(err error){
	if err = config.DB.Where("id = ?", id).First(customer).Error; err != nil {
		return err
	}
	   return nil
}

func PostCustomer(customer *Customer)(err error){
	if err := config.DB.Select("id","CustomerName","CustomerAddress","ContactName","Telephone","UserID","EmployeeId").Create(&customer).Error ; err != nil{
		return err
	}
		return nil
}

// func GetCustomer(customer *Customer, UserID string)(err error){
// 	if err = config.DB.Where("user_id = ?", UserID).First(customer).Error; err != nil {
// 		return err
// 	}
// 	   return nil
// }