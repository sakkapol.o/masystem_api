package models

type Employee struct {
	ID              int    `gorm:"primary_key:autoincrement" json:"id"`
	EmployeeName    string `gorm:"type:varchar(40)" json:"EmployeeName"`
	EmployeePostion string `gorm:"type:varchar(20)" json:"EmployeePostion"`
	UserID          string `gorm:"type:varchar(20)" json:"UserID"`
	IsDelete        int    `gorm:"type:int" json:"IsDelete"`
}

func (b *Issue) Employee() string {
	return "employee"
}
