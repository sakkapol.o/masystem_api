package models

type Issue struct {
	ID int `gorm:"primary_key:autoincrement" json:"id"`
	IssueDetail string `gorm:"type:varchar(900)" json:"IssueDetail"`
	IssueDate string `gorm:"type:varchar(100)" json:"IssueDate"`
	FixDetail string `gorm:"type:varchar(900)" json:"FixDetail"`
	FixDate string `gorm:"type:date" json:"FixDate"`
	IssueStatus string `gorm:"type:varchar(20)" json:"IssueStatus"`
	IssueLevel int `gorm:"type:int" json:"IssueLevel"`
	CustomerId string `gorm:"type:varchar(8)" json:"CustomerId"`
	EmployeeId string `gorm:"type:varchar(6)" json:"EmployeeId"`
	IsDelete int `gorm:"type:int" json:"IsDelete"`
}
// type DeleteIssue struct {
// 	ID int `json:"id"`
// 	IsDelete int `json:"IsDelete"`
// }
func (b *Issue) Issue() string {
	return "issue"
}
// func (b *DeleteIssue) UpdateIssue() string {
// 	return "DeleteIssue"
// }