package models

import (
	"basic_gin/config"
	//"fmt"
	_ "github.com/go-sql-driver/mysql"
)

func GetAllContractLists(contract *[]Contract)(err error){
	if err = config.DB.Where("is_delete = ?", "0").Find(&contract).Error; err != nil{
		return err
	}
	return nil
}

func GetContractDetails(contract *Contract, id string)(err error){
	if err = config.DB.Where("id = ?", id).First(contract).Error; err != nil {
		return err
	}
	   return nil
}

func PostContract(contract *Contract)(err error){
	if err := config.DB.Select("StartDate","EndDate","CustomerId").Create(&contract).Error ; err != nil{
		return err
	}
		return nil
}
//.Order("age desc, name").Find(&users)
func GetContract(contract *Contract, CustomerId string)(err error){
	if err = config.DB.Where("customer_id = ?", CustomerId).Order("end_date asc").Find(&contract).Error; err != nil {
		return err
	}
	   return nil
}