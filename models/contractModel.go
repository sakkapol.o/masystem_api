package models
type Contract struct {
	ID int `gorm:"primary_key:autoincrement" json:"id"`
	StartDate string `gorm:"type:date" json:"StartDate"`
	EndDate string `gorm:"type:date" json:"EndDate"`
	CustomerId string `gorm:"type:varchar(8)" json:"CustomerId"`
	IsDelete int `gorm:"type:int" json:"IsDelete"`
}

func (b *Contract) Contract() string {
	return "contract"
}