package routers

import (
	"basic_gin/controllers"
	"basic_gin/controllers/auth"

	//"time"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {

	r := gin.Default()
	//cors
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"*"}
	r.Use(cors.New(config))

	//issue
	r.GET("/api/issue", controllers.GetIssuesLists)
	r.GET("/api/issue/:id", controllers.GetIssuesDetails)
	r.POST("/api/issue", controllers.PostIssue)
	r.PUT("/api/issue/:id", controllers.UpdateIssue)
	r.DELETE("/api/issue/:id", controllers.DeleteIssue)
	//contract
	r.GET("/api/contract", controllers.GetContractLists)
	r.GET("/api/contract/:id", controllers.GetContractDetails)
	r.GET("/api/contract/customer/:CustomerId", controllers.GetContract)
	r.POST("/api/contract", controllers.PostContract)
	r.PUT("/api/contract/:id", controllers.UpdateContract)
	r.DELETE("/api/contract/:id", controllers.DeleteContract)
	//customer
	r.GET("/api/customer", controllers.GetCustomerLists)
	r.GET("/api/customer/:id", controllers.GetCustomerDetails)
	//r.GET("/api/customer/user/:UserID", controllers.GetCustomer)
	r.POST("/api/customer", controllers.PostCustomer)
	r.PUT("/api/customer/:id", controllers.UpdateCustomer)
	r.DELETE("/api/customer/:id", controllers.DeleteCustomer)
	//user
	r.GET("/api/user", controllers.GetUserLists)
	r.GET("/api/user/:id", controllers.GetUserDetails)
	r.POST("/api/user", controllers.PostUser)
	r.PUT("/api/user/:id", controllers.UpdateUser)
	r.DELETE("/api/user/:id", controllers.DeleteUser)
	//employee
	r.GET("/api/employee", controllers.GetAllEmployeeLists)
	//login
	r.POST("/api/login", auth.Login)

	return r

}
